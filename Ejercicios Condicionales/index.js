//Ejercicios
// ` `  > <  [] ||
/* 



10 - Que pida tres números y detecte si se han introducido en orden decreciente.
11 - Que pida 10 números y diga cuál es el mayor y cuál es el menor. 
12 - Que pida tres números e indicar si el tercero es igual a la suma del primero y el segundo.
13 - Que muestre un menú que contemple las opciones “Archivo”, “Buscar”, y “Salir”, en caso de que no se introduzca una opción correcta se notificará por pantalla.
14 - Que tome dos números del 1 al 5 y diga si ambos son primos.
15 - Que tome dos números y diga si ambos son pares o impares.
16 - Que tome tres números y diga si la multiplicación de los dos primeros es igual al tercero.
17 - Que tome tres números y diga si el tercero es el resto de la división de los dos primeros.
18 - Que muestre un menú donde las opciones sean “Equilátero”, “Isósceles” y “Escaleno”, pida una opción y calcule perímetro del triángulo seleccionado.
19 - Que pase de Kg a otra unidad de medida de masa, mostrar en pantalla un menú con las opciones posibles.
20 - Que lea un importe en bruto y calcule su importe neto, 
si es mayor de 15.000 se le aplicara un 16% de impuestos, en caso contrario se aplicara un 10%.

21 - Que calcule el sueldo que le corresponde al trabajador de una empresa que cobra 80.000 pesos anuales, 
el programa debe realizar los cálculos en función de los siguientes criterios: 
a)	Si lleva más de 10 años en la empresa se le aplica un aumento del 10%.
b)	Si lleva menos de 10 años, pero más que 5 se le aplica un aumento del 7%.
c)	Si lleva menos de 5 años, pero más que 3 se le aplica un aumento del 5%.
d)	Si lleva menos de 3 se le aplica un aumento del 3%.

22 – Primer Juego: hacer un programa donde declaremos una variable de tipo entero y la iniciamos con un valor entre 1 y 10. Por teclado pedimos ingresar un numero e indicamos si ha acertado el valor del número escondido (el valor de la variable inicializada).
 */

/* a) ingresá
https://getbootstrap.com/docs/4.5/examples/checkout/
b)Inspeccionar y buscar el input con el ID : first-Name

c) En la consola del Navegador escribir :
document.getElementById("firstName")

d)En la consola del Navegador escribir :
document.getElementsByTagName("input")

 --ese es el nodelist() */


//ejercicio1()
//ejercicio2()
//ejercicio3()
//ejercicio4()
//ejercicio5()
//ejercicio6()
//ejercicio7()
//ejercicio8()
ejercicio9()

//1 - Que pida un número del 1 al 12 y diga el nombre del mes correspondiente.
function ejercicio1() {
    const numeroElegido = Number(prompt("Escribe un número del 1 al 12"));
    const meses = [
        {
            month: "enero",
            num: 1,
        },
        {
            month: "febrero",
            num: 2,
        },
        {
            month: "marzo",
            num: 3,
        },
        {
            month: "abril",
            num: 4,
        },
        {
            month: "mayo",
            num: 5,
        },
        {
            month: "junio",
            num: 6,
        }
    ]

    function mesElegido(numeroElegido) {
        const mes = meses.find(mes => mes.num === numeroElegido);
        return mes.month
    }


    if (numeroElegido < 1 || numeroElegido > 12) {
        document.write("Hay un error con el valor indicado");
    } else {
        alert(`El numero indicado corresponde al mes de ${mesElegido(numeroElegido)} `);
    }
}

//2 - Que pida una letra y detecte si es una vocal.
function ejercicio2() {
    const letra = prompt("Escribe una letra")
    const vocales = ["a", "e", "i", "o", "u"]
    const esVocal = vocales.some(vocal => vocal === letra.toLowerCase());
    if (esVocal === true) {
        alert(`La letra ${letra} ingresada es una vocal`)
    } else {
        alert(`La letra ${letra} ingresada NO es una vocal`)
    }
}

/* 3 - Escribe un programa que acepte tres números, horas, minutos y segundos, 
y devuelva la hora que será dentro de un segundo, controlando que sea una hora correcta. */

function ejercicio3() {

    const day = new Date()
    const hora = day.getHours()
    const minuto = day.getMinutes()
    const segundo = day.getSeconds()
    horaActual = hora + ":" + minuto + ":" + segundo

    setTimeout("ejercicio3()", 1000)
    console.log(horaActual)
}

//4 - Que pida 3 números y los muestre en pantalla de menor a mayor.4 - Que pida 3 números y los muestre en pantalla de menor a mayor.

function ejercicio4() {
    const num1 = Number(prompt("Escribe un número"));
    const num2 = Number(prompt("Escribe un segundo número"));
    const num3 = Number(prompt("Escribe un tercer número"));

    const orden = [num1, num2, num3]
    orden.sort()

    document.write(`Detallamos los numeros ingresados de menor a mayor ${orden} `)

}

//5 - Que pida 3 números y los muestre en pantalla de mayor a menor.

function ejercicio5() {
    const num1 = Number(prompt("Escribe un número"));
    const num2 = Number(prompt("Escribe un segundo número"));
    const num3 = Number(prompt("Escribe un tercer número"));

    const numeros = [num1, num2, num3]
    const orden = numeros.sort().reverse()

    document.write(`Detallamos los numeros ingresados de mayor a menor ${orden} `)

}

//6- Que pida un número y diga si es positivo o negativo.

function ejercicio6() {
    const num = Number(prompt("Ingresa un número positivo o negativo"));
    const signo = Math.sign(num)

    if (signo === 1) {
        return document.write("El número ingresado es positivo")
    } else if (signo === 0) {
        return document.write("El número ingresado es neutro")
    } else {
        return document.write("El número ingresado es negativo")
    }

}


// 7 - Que solo permita introducir los caracteres S y N.

function ejercicio7() {

    const letra = prompt("Introduce una letra").toUpperCase()

    if (letra === "N" || letra === "S") {
        return document.write("Muchas gracias")
    } else {
        return document.write("solo se permiten los caracteres S y N")
    }
}

//8 - Que pida un número y diga si es mayor de 100. 

function ejercicio8() {
    const num = Number(prompt("Ingresa un número"));
    const numMayor = 100;

    if (num > numMayor) {
        return alert(`Ingresaste un número mayor a ${numMayor} `)
    } else if (num < numMayor) {
        return alert(`Ingresaste un número menor a ${numMayor} `)
    } else {
        return alert(`Ingresaste un número igual a ${numMayor} `)
    }

}

//9 - Que pida tres números y detecte si se han introducido en orden creciente.

function ejercicio9() {

    const num1 = Number(prompt("ingresa primer numero"));
    const num2 = Number(prompt("ingresa segundo numero"));
    const num3 = Number(prompt("ingresa tercer numero"));

    const conjuntoNum = [num1, num2, num3];
/*     const conjuntoNumCopia = [...conjuntoNum]
    const ordenCreciente = conjuntoNumCopia.sort(); */

    let estaEnCreciente = true;

 /*    conjuntoNum.forEach((num, i) => {
        if (ordenCreciente[i] !== num) {
            estaEnCreciente = false
        }
    }); */

        conjuntoNum.forEach((num, i) => {
           if(num >= conjuntoNum[i + 1] ) {
               estaEnCreciente = false;
           }
        })

    if (estaEnCreciente === true) {
        return alert("ingresaste los numeros en orden creciente")
    } else {
        return alert("no ingresaste los numeros en orden creciente")
    }

}
