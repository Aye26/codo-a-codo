/* Ejercicio 1 Generar número aleatorio dentro de un rango

Calcular un número random o aleatorio dentro de un rango en Javascript no es tan trivial 
como en otros lenguajes. 

La función que usaremos (Math.random())
nos devuelve un número aleatorio con decimales entre 0 y 1. 

 Luego nosotros, haciendo uso de sumas y redondeos debemos transformar
ese número aleatorio entre 0 y 1 a un número aleatorio entre 25 y 75, por ejemplo.

Para ello, yo utilizo la fórmula que indico a continuación. 
La copio, la pego y sustituyo los valores. Es una fórmula fácil de razonar, pero francamente, cuando la necesito,
la copio y la pego:

Math.floor(Math.random() * (MAX - MIN + 1)) + MIN;

Como ejemplo, si deseamos generar un número aleatorio entre 10 y 5, la sentencia sería

Math.floor (Math.random() * (MAX - MIN + 1)) + MIN;            
*/


// Retorna un número aleatorio entre min (incluido) y max (excluido) -- Math.random() devuelve numeros aleatorio con decimales
//con Math.floor logro que los numeros sean enteros
// si multiplico el random por *10 por ejemplo, genera que los numeros aleatorios sean entre
// el 0 incluido y el 10 excluido
//Si quiero que cuente los numeros hasta el 10, debo sumar 1

//const a = Math.floor (Math.random() *10) +1 ;
//alert (a);

const max = 75;
const min = 25;
alert (Math.floor(Math.random() * (max - min)) + min);
